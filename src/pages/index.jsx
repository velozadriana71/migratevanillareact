import React, { useEffect, useState } from "react";
import { list, deleteData } from "../services/task.services.js";
import TaskItem from "../components/TaskItem.jsx";

import "../styles/Task.css";
const Task = () => {
  const [tasks, setTasks] = useState([]);

//Obtenemos todos los task
  const getData = async () => {
    const result = await list();
    if (!result.status)
      return alert("Something went grong! -" + result.message);

    setTasks(result.data);
  };

  //Manejador eliminar
  const handleDelete = async (id, title) => {
    const respuesta = confirm(`Do you want to delete this task? "${title}"?`);
    if(!respuesta) return;

    const result = await deleteData(id);
    if(!result.status) return alert("Something went grong! -" + result.message);

    getData();
  };

  //Al cargar la página
  useEffect(() => {
    getData();
  }, []);

  if (tasks === undefined || tasks === null || tasks.length === 0)
    return <div>Loading...</div>;

  return (
    <div className="task-container">
      {tasks.map((task) => (
        <TaskItem
          id={task.id}
          title={task.title}
          description={task.description}
          key={task.id}
          hDelete={handleDelete}
        />
      ))}
    </div>
  );
};
export default Task;
