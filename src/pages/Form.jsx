import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { get, create, update } from "../services/task.services";
import "../styles/Form.css";

const Form = () => {
  const { id } = useParams();
  const navigate = useNavigate();

  //Estados
  const [txtTitle, settxtTitle] = useState("");
  const [txtDescription, settxtDescription] = useState("");

  //Obtenemos task
  const getTask = async () => {
    const result = await get(id);
    if (!result.status)
      return alert("Something went grong! -" + result.message);

    settxtTitle(result.data.title);
    settxtDescription(result.data.description);
  };
  //Al enviar la info
  const handleSubmit = async(e) =>{
      e.preventDefault();

      const task = {
          title: txtTitle,
          description: txtDescription,
      };
  
      let result;
      if(id){
          result = await update(id, task);
      } else{
         result = await create(task);
      }
  
      if(!result.status) return alert("Something went grong! -" + result.message);

      navigate(-1);
  };

  //Al cargar la pagina
  useEffect(() => {
    if (id) getTask();

    //Al cerrar la pagina
     return () =>{
      settxtTitle("");
      settxtDescription("");
     }
  }, []);

  return (
    <div className="form-container">
      <div className="form-title"> {id ? "Update" : "Save"} Task</div>
      <form className="form" onSubmit={handleSubmit}>
        <input type="text" placeholder="Title" className="form-input" onChange={ (e) => settxtTitle(e.target.value)} value={txtTitle} />
        <input type="text" placeholder="Descripción" className="form-input"  onChange={ (e) => settxtDescription(e.target.value)} value={txtDescription} />

        <button type="submit" className="btn"> {id ? "Update" : "Save"}
        </button>
      </form>
    </div>
  );
};
export default Form;
