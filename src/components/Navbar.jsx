import React from "react";
import { Link } from "react-router-dom";

export const Navbar = () => {
  return (
    <header>
            <Link to="/" className="title1">JS APP</Link>
            <Link to="/add" className="title">ADD TASK</Link>
    </header>
  );
};

export default Navbar;
